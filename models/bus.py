
from os import stat
from main import database

class Bus(database.Model):
    __tablename__='Buses'

    id = database.Column(database.Integer,primary_key=True)
    modelo = database.Column(database.Integer, nullable=False)
    capacidad = database.Column(database.Integer, nullable=False)
    placa = database.Column(database.String, nullable=False)
    marca = database.Column(database.String, nullable=False)

    def __init__(self, modelo,capacidad,placa,marca):
        self.modelo=modelo
        self.capacidad=capacidad
        self.placa=placa
        self.marca=marca

    def create(self):
        database.session.add(self)
        database.session.commit()

    @staticmethod
    def delete(id_to_delete):
        Bus.query.filter_by(id=id_to_delete).delete()
        database.session.commit()

    @staticmethod 
    def update_model(id_to_update,new_model):
        bus = Bus.query.get(id_to_update)
        bus.modelo = new_model
        database.session.commit()
        
    @staticmethod
    def get_all():
        return Bus.query.all()

    @staticmethod
    def get_model(model):
        return Bus.query.filter_by(modelo=model)

class Trayecto(database.Model):
    __tablename__='Trayectos'
    id = database.Column(database.Integer,primary_key=True)
    id_estacion = database.Column(database.Integer, database.ForeignKey('Estaciones.id'))
    id_bus = database.Column(database.Integer, database.ForeignKey('Buses.id'))
    nombre = database.Column(database.String, nullable=False)
    valor_trayecto = database.Column(database.Integer, nullable=False)

    @staticmethod 
    def get_all():
        print(Trayecto.query.all())
        return Trayecto.query.all()


    @staticmethod
    def test_join():
        resultado = database.session.query(Bus, Trayecto).join(Trayecto, Bus.id==Trayecto.id_bus).all()
        primer_registro = resultado[0] #tupla (Bus,Trayecto)
        placa_bus = primer_registro[0].placa
        nombre_trayecto = primer_registro[1].nombre
        print("placa: ",placa_bus," nombre_trayecto: ",nombre_trayecto)
""" 
class Viajes(database.Model):
    __tablename__ = "Viajes"
    id = database.Column(database.Integer,primary_key=True)
    id_pasajero = database.Column(database.Integer,database.ForeignKey('Pasajeros.id'))
    id_trayecto = database.Column(database.Integer,database.ForeignKey('Trayectos.id'))
    inicio = database.Column(database.String)
    fin =  database.Column(database.String)
    id_pasajero = database.relationship('Pasajeros',foreign_keys='Pasajeros.id')
    id_trayecto = database.relationship('Trayectos',foreign_keys='Trayectos.id')

    @staticmethod
    def example_join(id_pasajero):
        return Viajes.query.join(pasajeros,id_pasajero=pasajeros.id)
"""