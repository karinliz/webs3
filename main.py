from flask import Flask,request,make_response,redirect, render_template
from flask.wrappers import Response
from Crud import Crud
import json #para poder dar formato json 
from datetime import date, datetime
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__) #instancia de Flask
#postgresql://<nombre_usuario>:<password>@<host>:<puerto>/<nombre_basededatos>

app.config['SQLALCHEMY_DATABASE_URI'] ="postgresql://postgres:admin@localhost:5432/transportes_S3"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(32)

database = SQLAlchemy(app)

from models.bus import *

#primer ruta 
@app.route('/')#ruta en el navegador
def hello():#funcion que se realiza cuando se acceda a esa ruta
    #user_ip = request.remote_addr # solicitando direccion ip al navegador (cliente)
    #print(user_ip)
    dic_prueba={"Modelo":1990,"Capacidad":100,"Fabricante":"Mazda"}
    Trayecto.test_join()
    return render_template("index.html",prueba="enviado desde main.py",prueba2="var prueba 2",bus=dic_prueba)

@app.route("/login", methods=["GET","POST"])
def login():
    nombre_usuario="admin"
    password="admin"
    if(request.method=="GET"):
        return render_template("login.html")
    if(request.method=="POST"):
        username=request.form.get("username")
        passwd = request.form.get("passwd")
        if(username==nombre_usuario and passwd==password):
            response = make_response(redirect("/get_buses"))
            return response
        else:
            return render_template("login.html",acceso="incorrecto")


@app.route("/get_bus")
def leer_bus():

    crud = Crud("localhost","transportes_S3", "postgres","admin")
    buses = crud.leer_buses()
    primer_registro = buses[0]
    #convertir a string
    print(primer_registro)
    la_ip = request.cookies.get('user_ip')

    #registro = "id="+str(primer_registro[0])+" modelo="+str(primer_registro[1])+\
    #    " capacidad="+str(primer_registro[2])+" placa="+primer_registro[3]+\
    #        " marca="+primer_registro[4]
    #retornar string
    retorno = json.dumps({"id":primer_registro[0], "modelo":primer_registro[1],
     "capacidad":primer_registro[2],"placa":primer_registro[3],"marca":primer_registro[4],
     "ip_navegador":la_ip})
    return retorno

@app.route("/get_buses") 
def leer_buses():
    """
    crud = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = crud.leer_buses()#buses es una lista de tuplas
    respuesta = [] #sera una lista de diccionarios
    for registro in buses: 
        respuesta.append({"id":registro[0], "modelo":registro[1],
     "capacidad":registro[2],"placa":registro[3],"marca":registro[4]})
    """
    #bus = Bus(2021,100,'asdfasdf','Volvo')
    #bus.create()
    #Bus.delete(53)
    Bus.update_model(54,1990)
    return render_template("get_buses.html",buses = Bus.get_all())

@app.route("/get_viaje") 
def leer_viaje():
    crud = Crud("localhost","transportes_S3", "postgres","admin")
    viajes = crud.leer_viajes()
    primer_registro = viajes[0] #primera tupla
    print("el primer registro=", primer_registro)
    hora_inicio = primer_registro[3]#posicion tres de la tupla
    hora_fin = primer_registro[4] #posicion 4 de la tupla
    hora_inicio_string = hora_inicio.strftime("%d/%m/%Y - %H:%M:%S") #convirtiendo datetime a string
    hora_fin_string = hora_fin.strftime("%d/%m/%Y - %H:%M:%S")    
    print(hora_inicio)
    print(type(hora_inicio))
    print(hora_fin)
    print(type(hora_fin)) 
    print("la diferencia en minutos=", (hora_fin - hora_inicio).total_seconds() / 60)
    respuesta = json.dumps({"id":primer_registro[0] ,"id_pasajero":primer_registro[1],
     "id_trayecto":primer_registro[2],
    "hora inicio":hora_inicio_string,"hora fin":hora_fin_string})
    return respuesta

@app.route("/get_viajes") 
def leer_viajes():
    crud = Crud("localhost","transportes_S3", "postgres","admin")
    viajes = crud.leer_viajes()
    respuesta = []
    for registro in viajes:

        hora_inicio = registro[3]#posicion tres de la tupla
        hora_fin = registro[4] #posicion 4 de la tupla
        hora_inicio_string = hora_inicio.strftime("%d/%m/%Y - %H:%M:%S") #convirtiendo datetime a string
        hora_fin_string = hora_fin.strftime("%d/%m/%Y - %H:%M:%S")    
        respuesta.append({"id":registro[0] ,"id_pasajero":registro[1],
        "id_trayecto":registro[2],
        "hora inicio":hora_inicio_string,"hora fin":hora_fin_string})
    
    respuesta = json.dumps(respuesta)
    
    return respuesta

if __name__=="__main__": #punto de partida, donde python empieza a ejecutar
	while(True):
		print("starting web server")
		app.run(debug=True,host='0.0.0.0')#arranca el servidor, 0.0.0.0->localhost
        #en el navegador se accede localhost:5000/